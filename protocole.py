from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP


class AsymetricRSA:
    def __init__(self, size = 2048, key = None):
        if key == None:
            self.__private_key = RSA.generate(size)
        else:
            self.__private_key = RSA.import_key(key)
        
        # Le message venant d'une tierce personne, il va être déchiffré par la clé privée
        self.decrypter = PKCS1_OAEP.new(self.__private_key) 
        
        # Public Key client/host, encryter et envoyer msg asymetric
        self.encrypter: Rsakey = ...
    
    def get_public_key(self):
        return self.__private_key.public_key()
    
    def regenerate_key(self, size = 2048):
        try:
            self.__private_key = RSA.generate(size)
            self.decrypter = PKCS1_OAEP.new(self.__private_key)
            return True
        except:
            return False
        
    def set_public_key_extern(self, public_key):
        self.public_key_extern = RSA.import_key(public_key)
        self.encrypter = PKCS1_OAEP.new(self.public_key_extern)
        
    def encode(self, message) -> bytes:
        if self.encrypter != Ellipsis:
            if type(message) == str:
                return self.encrypter.encrypt(message.encode())
            elif type(message) == bytes:
                return self.encrypter.encrypt(message)
            else:
                raise TypeError("str or bytes are accepted. Message type :", type(message))
    
    def decode(self, message) -> bytes:
        if type(message) == str:
            return self.decrypter.decrypt(message.encode())
        elif type(message) == bytes:
            try:
                return self.decrypter.decrypt(message)
            except ValueError:
                pass

        else:
            raise TypeError("str or bytes are accepted. Message type :", type(message))


class AsymetricRSAPublicKey(AsymetricRSA):
    def __init__(self, key):
        self.public_key = RSA.import_key(key)
        
        # Le message venant d'une tierce personne, il va être déchiffré par la clé privée
        self.encrypter = PKCS1_OAEP.new(self.public_key) 
    
    def get_public_key(self):
        return self.public_key

    def regenerate_key(self, *argv):
        return False
    
    def decode(self, *argv):
        raise AsymetricRSAPublicKeyException

class AsymetricRSAPublicKeyException(Exception):
    def __init__(self, *args):
        super().__init__(*args)
