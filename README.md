Package Socker
==============

# Introduction

Ce module a pour avantage de combiner client et hôte en même temps. C'est le module `socker.py` qui est au centre du reseautage avec 4 classes principales (Socker, SimpleSocker, SockerClient, SimpleSockerClient) et 2 classes d'arrangements (User, Message). Il a pour aider à sa tâche les modules `protocole` (avec les classes AsymetricRSA, AsymetricRSAPublicKey, AsymetricRSAPublicKeyException) et `utils`.

Ce module donne la possibilité de partager des informations avec le chiffrement RSA et Fernet car l'un et l'autre a ses avantages et inconvénients que vous verrez plus loin dans la partie `Modules Socker`.

Pour la licence, référez vous au fichier LICENCE annexe à ce README. Le package est sous licence `GNU General Public License v3.0`.

# Sommaire

1. **Introduction**
2. **Requis**
 - Requis
 - Licences Tierces
3. **Ensemble des modules**
 - Module Utils
 - Module Protocole
 - Module Socker
 - Module Main
# Requis

Les modules importent depuis l'extérieur les modules `pycryptodome` et `cryptography`.
```bash
$ pip install -r requirements
```

## Licence des modules externes

* **pycryptodome** - BSD license
* **cryptography** - BSD and APACHE licence

# Ensemble des modules

Chaque module sera expliquée dans cette section.

* utils
* protocole
* socker
* main

Les modules utilisés dans le programme sont :

* Crypto.PublicKey -> RSA
* Crypto.Cipher -> PKCS1_OAEP
* os -> path
* socket -> socket, AF_INET, SOCK_STREAM
* threading -> Thread
* time -> time, sleep
* sys -> param
* json
* cryptography.fernet -> Fernet

## Module utils

Ce modules n'a pas de classes spécifiques mais rassemble toutes les petites fonctions qui sont utiles et réutilisables.

### yieldfile
    (name, separator, type_r_or_rb)
* **name** - Le nom du fichier et son chemin
* **separator** - Le nombre de seek de déplacement dans le fichier
* **type_r_or_rb** - Le type d'ouverture que doit procéder la fonction, soit `r` soit `rb`

C'est une fonction génératrice qui renvoie tout le fichier par étape jusqu'à arriver au bout de ce dernier. Le séparateur est important si vous voulez envoyer par bout de ce fichier 

## Module protocole

Ce module appelle le paquet `pycryptodome` à installer pour la bonne fonction du module

### class AsymetricRSA
    (size = 2048, key = None)

Cette classe permet de générer, encoder et décoder grace à la clef privée générée à partir des arguments données en paramètres

* **size** - Taille en bits pour la génératon de la clef
* **key** - Clé extérieur si une avait déjà été générée

#### get_public_key
Retourne la clef public à partir de la paiur de cléfs générée au début

#### regenerate_key
    (size)
Regénère entièrement la paire de clefs, assez déconseillé surtout pendant la transaction entre deux ou plusieurs périphériques

#### set_public_key_extern
    (key)
Récupère la clef public du périphérique distant. __Déconseillé car la class AsymetricRSAPublicKey s'en occupe.__

#### encode
    (message)
* **message**:str or bytes (`raise TypeError`)
Encode le message avec la clef public du périphérique distant et retourne en `bytes`. __Déconseillé car la class AsymetricRSAPublicKey s'en occupe.__

#### decode
    (message)
* **message**:str or bytes
Decode le message avec la clef privée générée au tout début et retourne le résultat en `bytes`.

### class AsymetricRSAPublicKey
    (key)
La classe `AsymetricRSAPublicKey` est la classe fille de `AsymetricRSA`. La différence étant que la fonction `regenerate_key` retourne `False` et `decode` lève l'exception `AsymetricRSAPublicKeyException`

### class AsymetricRSAPublicKeyException
    (*args)
Classe fille de la classe Exception, utilisée notemment dans la class AsymetricRSAPublicKey pour le décodage avec une clef public

## Module socker

C'est le module qui va s'occuper de la communication entre les périphériques comprenant 6 classes qui seront détaillées ci-dessous `Message`, `User`, `Socker`, `SimpleSocker`, `Sockerclient` et `SimpleSockerClient`.

### class Message
    (ctx)
* **ctx** - Contexte, ou plutôt message de préférence en `bytes` ou `str`.
C'est dans cette class que les receptions se font en générale.

#### get_type
Retourne le type du ctx

### class User
    (id, socket)
* **id** - De tout type, il doit être facile à utiliser et récupérable. Il est recommandé d'utiliser les types `str` ou `int` comme id

Centralise toutes les informations d'un client distant. Il permet d'exporter l'intégralité des données dans un seul objet, gardée dans la RAM.

__Variable de classe__
 - rsa_key -> RsaKey
 - id -> Any
 - sock -> socket
 - data -> list(Any)
 - infos -> tuples(Any)

#### close
Ferme le socket de l'utilisateur.

### class Socker
    (hostname, port, data_limit, timeout_connection, verbose)
* **hostname** (Default: "") - Nom de domaine du server
* **port** (Default: 5051) - Port du serveur
* **data_limit** (Default: 24) - Limite le nombre de données stockable dans un utilisateur
* **timeout_connection** (Default: 10) - Le temps limite acceptable d'attente pour toute fonction socket
* **verbose** (Default: False) - Explicite ou non les events, exceptions exclues

C'est la classe principale du module.
Clef en main d'un échangeur de packets à plusieurs utilisateurs
Le service propose un échange asymétrique grace au protocole RSA.
Il est impossible pour l'instant d'extraire la cléf privée, elle reste inconnue même pour l'utilisateur host.

__Variable de classe__
 - hostname
 - port
 - verbose
 - data_limit
 - host
 - users
 - rsa_key
 - accept_socket

__Variable Privée de classe__
 - nbr_id
 - acpth

#### \_\_accept
Boucle qui accepte les connexions tant que `accept_socket` est vraie ou qu'une exception soit levée.

Les étapes de la déclaration de l'utilisateur sont :
1. Acceptation de la connexion dans `sock`
2. Concaténation et Récupération de l'ID avec `__nbr_id`
3. Création de l'utilisateur.
```py
client = User(id, sock)
```
4. Envoie de la cléf publique de Host et receptionde celle de Client.
5. Déclaration définitive de User et démarrage de son focus dans un nouveau Thread appelant la fonction `__recv`.

#### \_\_send
    (obj, id_socket)
* **obj** - Objet à envoyer en `str` et `bytes`
* **id_socket** - C'est le `user.id` à donner pour récupérer le `User` et lui envoyer personnellement l'objet en binaire chiffré avec la clef RSA.

Cette fonction permet d'envoyer un objet simple et limité à 234 octets.

#### \_\_sendbinaryfile
    (filename, id_socket)
* **filename** - Lien vers le fichier binaire en question
* **id_socket** - C'est le `user.id` à donner pour récupérer le `User` et lui envoyer personnellement le fichier binaire chiffré avec le module Frenet.

Envoie un fichier binaire crypté.
__/!\\ Le module n'est pas encore parfait et quelques données se perdent en chemin, n'hésitez pas à partager vos solutions !__

```py
from cryptography.fernet import Fernet
from utils import yieldfile
```

#### \_\_recv
    (id_socket)
* **id_socket** - C'est le `user.id` à donner pour récupérer le `User` et recevoir personnellement le message binaire chiffré avec le module Frenet.

Recupère en boucle, tant que `accept_socket` est vraie, les objets envoyés par l'utilisateur avant de l'entreposer dans `user.data`.

#### send
    (obj, id_socket)
* **obj** - Objet à envoyer en `str` et `bytes`
* **id_socket** - C'est le `user.id` à donner pour récupérer le `User` et lui envoyer personnellement l'objet en binaire chiffré avec la clef RSA.

Démarre un nouveau Thread et appelle \_\_send pour libérer le Thread 1.

#### sendbinaryfile
    (filename, id_socket)
* **filename** - Lien vers le fichier binaire en question
* **id_socket** - C'est le `user.id` à donner pour récupérer le `User` et lui envoyer personnellement le fichier binaire chiffré avec le module Frenet.

Démarre un nouveau Thread et appelle \_\_sendbinaryfile pour libérer le Thread 1.

### class SimpleSocker
    (hostname, port, data_limit, timeout_connection, verbose)
* **hostname** (Default: "") - Nom de domaine du server
* **port** (Default: 5051) - Port du serveur
* **data_limit** (Default: 24) - Limite le nombre de données stockable dans un utilisateur
* **timeout_connection** (Default: 10) - Le temps limite acceptable d'attente pour toute fonction socket
* **verbose** (Default: False) - Explicite ou non les events, exceptions exclues

class fille de `Socker`

Clef en main d'un échangeur de packets à plusieurs utilisateurs
Le service ne propose pas de clef de chiffrement, c'est le but de ce dernier, utilisable pour des connexions étant très sûres, donc sans besoin de chiffrements, il est tout de même recommandé d'utiliser `Socker` qui n'envoient pas les données à la même vitesse mais permet d'avoir un chiffrement entre ls utilisateur et l'hôte.

Ce sont les mêmes fonctions avec la particularité de ne pas avoir besoin de RSA ou de Frenet.

__Variable de classe__
 - hostname
 - port
 - verbose
 - data_limit
 - host
 - users
 - ~~rsa_key~~
 - accept_socket

__Variable Privée de classe__
 - nbr_id
 - acpth


### class SockerClient
    (hostname, port)

* **hostname** - Nom de domaine du server distant Socker
* **port** - Port du server distant Socker

Cette classe est la partie utilisateur de `Socker`. C'est la class qui va faire le lien entre le client et l'hôte distant

__Variable de classe__
 - client
 - proto_host
 - rsa_key

#### send
    (message)
* **message** - Message (objet) à envoyer en `str` et `bytes`

Envoie au serveur un message crypté avec la cléf reçu `proto_host`

#### recv
    (size)
* **size** - Taille en bits de la taile maximale de réception de `socket.recv(size)`

Récupère les objets envoyés par l'hôte.

#### recvfile
    (size, step)
* **size** - Taille en bits de la taile maximale de réception de `socket.recv(size)`
* **step** - Taille en bits de l'écriture du fichier à chaque reception

Récupère les fichiers envoyés par l'hôte.

### class SimpleSockerClient
    (hostname, port)

* **hostname** - Nom de domaine du server distant Socker
* **port** - Port du server distant Socker

class fille de `SockerClient`

Cette classe est la partie utilisateur de `Socker`. C'est la class qui va faire le lien entre le client et l'hôte distant __sans chiffrement__. Elle s'utilise en paralèlle de `SimpleSocker`.

__Variable de classe__
 - client
 - ~~proto_host~~
 - ~~rsa_key~~


## Module Main

C'est le module ici utilisé afin de tester les paramètres et donner une première interface.

Il importe directement socker ainsi que les argv de sys comme qui suit :

```python
from socker import Socker, SimpleSocker, SockerClient, SimpleSockerClient
from sys import argv as param
```

Son utilisation la plus simple se fait depuis le terminal de deux façon différentes :

__host__

```bash
$ python -m main host
```
Et avec toutes les options activées
```bash
$ python -m main host -v --no-rsa
```

__client__

_Ici, le serveur est sur la même machine_
```bash
$ python -m main client 127.0.0.1 5051
```
Et sans chiffrement :
```bash
$ python -m main client 127.0.0.1 5050 --no-rsa
```


```python
def main(host, port):
    client = Socker(host, port)


if __name__ == "__main__":
    if len(param) >= 1:
        if param[1] == 'host':
            verbose = False
            
            if '-v' in param or '--verbose' in param:
                verbose=True
            if '--no-rsa' in param:
	            host = SimpleSocker(verbose=verbose)
            else:
                host = Socker(verbose=verbose)
        
        elif param[1] == 'client':
            if "--no-rsa" in param:
                try:
                    SimpleSockerClient(
                        param[2], #hostname
                        param[3]  #port
                    )
                    
                except IndexError:
                    print("Usage: main.py client HOST PORT <option>")
            else:
                try:
                    main(
                        param[2], #hostname
                        param[3]  #port
                    )
                    
                except IndexError:
                    print("Usage: main.py client HOST PORT <option>")
                
    else:
        print('')
        print('Usage: main.py [host/client]')
```

# Divers

1. Le port pour la connexion sécurisé par défaut est 5051 et celle non sécurisée 5050