from os import path

def yieldfile(name, separator, type_r_or_rb):
    with open(name, type_r_or_rb) as file:
        b = path.getsize(name)
        a = 0
        while a < b:
            file.seek(a)
            yield file.read(separator-1)
            a+=separator