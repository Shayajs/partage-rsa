from socket import socket, AF_INET, SOCK_STREAM
from threading import Thread
from time import time, sleep
from protocole import AsymetricRSA, AsymetricRSAPublicKey
from sys import argv as param
from utils import yieldfile
from os import path
import json
from cryptography.fernet import Fernet

class Message:
    """Cet objet permet une centralisation des informations d'un message envoyé, encore à ses balbutiement"""
    def __init__(self, ctx):
        self.time = time()
        self.ctx = ctx
        
    def get_type(self):
        return type(self.ctx)
    
    def __str__(self):
        return str(self.ctx) + f" [{str(type(self.ctx))}]"


class User:
    """Centralise toutes les informations d'un client distant.
    Il permet de pouvoir exporter l'intégralité dans un seul objet
    """
    def __init__(self, id, socket: socket):
        self.rsa_key = ...
        self.id = id
        self.sock: socket = socket
        self.data = []
        self.infos = ...
    
    def close(self):
        self.sock.close()
        
    def delete():
        self.__del__()


class Socker:
    """
    cléf en main d'un échangeur de packets à plusieurs utilisateurs
    hostname >> Nom de domaine du server
    port >> port du serveur
    data_limite >> Limite le nombre de données stockable dans un utilisateur
    
    Le service propose un échange asymétrique grace au protocole RSA.
    Il est impossible pour l'instant d'extraire la cléf privée, elle reste inconnue même pour l'utilisateur host
    """
    
    def __init__(self, hostname = "", port = 5051, data_limit = 24, timeout_connection = 10, verbose = False):
        
        self.verbose = verbose
        
        # Data host
        self.hostname = hostname
        self.port = port
        self.data_limit = data_limit
        
        if verbose == True: print("Host start...")
        
        # Host starter
        self.host = socket(AF_INET, SOCK_STREAM)
        self.host.bind((self.hostname, self.port))
        self.host.listen(5)
        self.host.settimeout(timeout_connection)

        if verbose == True: print(f"Host Started ({self.host.getsockname()}, timeout = {timeout_connection}")
        
        self.users = {} # id : User
        
        self.rsa_key = AsymetricRSA()
        if verbose == True: print("RSA Key created")
        
        # Donner un identifiant unique simple pour s'y retrouver
        self.__nbr_id = 0
        
        # Le looper pour le Thread
        self.accept_socket = True
        
        self.__acpth = Thread(None, self.__accept)
        self.__acpth.start()
        if verbose == True: print("Host deployed !")
        
    def __accept(self):
        
        while self.accept_socket:
            
            try:
                #Accept Connexion
                sock, infos = self.host.accept()
                self.__nbr_id += 1
                id = self.__nbr_id
                
                # Create user
                client = User(id, sock)
                client.infos = infos
                if self.verbose == True: print(f"{infos} connected with id {id}")
                
                # Envoie de la clé publique
                client.sock.send(self.rsa_key.get_public_key().export_key())
                
                # Reception de sa cléf publique
                client.rsa_key = AsymetricRSAPublicKey(client.sock.recv(5120))
                sleep(0.25)
                if self.verbose == True: print(f"RSA Keys send and received (id : {id})")
                
                # Enregistre le client distant
                self.users[self.__nbr_id] = client
                
                # Démarre le loop script de reception
                Thread(None, self.__recv, None, (id,)).start()
                
            except TimeoutError:
                pass
            
            except Exception as e:
                raise e
    
    def send(self, obj, id_socket):
        Thread(None, self.__send, None, (obj, id_socket)).start()
    
    def sendfile(self, filename, id_socket):
        Thread(None, self.__sendbinaryfile, None, (filename, id_socket)).start()
    
    def __sendbinaryfile(self, filename, id_socket):
        
        client: socket = self.users[id_socket].sock
        
        #Envoie unique en JSON
        json_infos = json.dumps({"name": filename, "size": path.getsize(filename)})
        client.send(self.users[id_socket].rsa_key.encode(json_infos.encode()))
        
        sleep(0.25)
        
        key = Fernet.generate_key()
        fernet = Fernet(key)
        
        client.send(self.users[id_socket].rsa_key.encode(key))
        sleep(0.25)
        for i in yieldfile(filename, 2048, "rb"):
            datasend = fernet.encrypt(i)
            client.send(datasend)
        if self.verbose == True: print("Finish send file")
    
    def __send(self, obj, id_socket):
        
        # Récupération du socket client
        client: socket = self.users[id_socket].sock
        msg = ...
        
        if type(obj) == str:
            msg = obj.encode()
        if type(obj) == bytes:
            msg = obj
        else:
            msg = str(obj).encode()
        
        # Encodage et envoie
        msg_finall = self.users[id_socket].rsa_key.encode(msg)
        client.send(msg_finall)
    
    def __recv(self, id_socket):
        
        user: User = self.users[id_socket]
        socket: socket = user.sock
        try:
            while self.accept_socket:
                msg_raw = socket.recv(5120)
                msg_decr = self.rsa_key.decode(msg_raw)
            
                self.users[id_socket].data.append(Message(msg_decr))
                """
                users {
                    id : User {
                        Data : [
                            Message[ctx: obj]
                        ]
                    }
                }
                """
                
                # Eviter les dépassement de mémoire RAM
                if len(self.users[id_socket].data) > self.data_limit:
                    self.users[id_socket].data.pop(0)
                    
                try:
                    if self.verbose == True: print(f"id {self.users[id_socket].id} MSG  - {msg_decr.decode()}")
                except AttributeError:
                    socket.close()

                except Exception as e:
                    print(f"id {id} error - {e.__str__()}")
                    socket.close()
                    
        except OSError:
            if self.verbose == True: print(f"Connection finished with {user.infos[0]} - id {user.id}")
            
    def get_user_data(self, id):
        return self.users[id_socket].data

    def get_user(self, id):
        return self.users[id_socket]

class SimpleSocker(Socker):
    def __init__(self, hostname = "", port = 5050, data_limit = 24, timeout_connection = 10, verbose = False):
        """
        cléf en main d'un échangeur de packets à plusieurs utilisateurs
        hostname >> Nom de domaine du server
        port >> port du serveur
        data_limite >> Limite le nombre de données stockable dans un utilisateur
        
        Même service mais sans chiffrements
        """
    
        
        self.verbose = verbose
        
        # Data host
        self.hostname = hostname
        self.port = port
        self.data_limit = data_limit
        
        if verbose == True: print("Host start...")
        
        # Host starter
        self.host = socket(AF_INET, SOCK_STREAM)
        self.host.bind((self.hostname, self.port))
        self.host.listen(5)
        self.host.settimeout(timeout_connection)

        if verbose == True: print(f"Host Started ({self.host.getsockname()}, timeout = {timeout_connection}")
        
        self.users = {} # id : User
        
        # Donner un identifiant unique simple pour s'y retrouver
        self.__nbr_id = 0
        
        # Le looper pour le Thread
        self.accept_socket = True
        
        self.__acpth = Thread(None, self.__accept)
        self.__acpth.start()
        if verbose == True: print("Host deployed !")
        
        
    def __recv(self, id_socket):
        
        user: User = self.users[id_socket]
        socket: socket = user.sock
        
        while self.accept_socket:
            msg_raw = socket.recv(5120)
        
            self.users[id_socket].data.append(Message(msg_raw))
            """
            users {
                id : User {
                    Data : [
                        Message[ctx: obj]
                    ]
                }
            }
            """
            
            # Eviter les dépassement de mémoire RAM
            if len(self.users[id_socket].data) > self.data_limit:
                self.users[id_socket].data.pop(0)
                
            try:
                if self.verbose == True: print(f"id {self.users[id_socket].id} MSG  - {msg_raw.decode()}")
            except Exception as e:
                print(f"id {id} error - {e.__str__()}")
                socket.close()
                
    def sendfile(self, filename, id_socket):
        Thread(None, self.__sendbinaryfile, None, (filename, id_socket)).start()
        
    def __sendbinaryfile(self, filename, id_socket):
        
        client: socket = self.users[id_socket].sock
        
        json_infos = json.dumps({"name": filename, "size": path.getsize(filename)})
        client.send(json_infos.encode())
        
        for i in yieldfile(filename, 5120, "rb"):
            client.send(i)
    
    def __send(self, obj, id_socket):
        
        # Récupération du socket client
        client: socket = self.users[id_socket].sock
        msg = ...
        
        if type(obj) == str:
            msg = obj.encode()
        if type(obj) == bytes:
            msg = obj
        else:
            msg = str(obj).encode()
        
        client.send(msg)

    def __accept(self):
        
        while self.accept_socket:
            
            try:
                #Accept Connexion
                sock, infos = self.host.accept()
                self.__nbr_id += 1
                id = self.__nbr_id
                
                # Create user
                client = User(id, sock)
                client.infos = infos
                if self.verbose == True: print(f"{infos} connected with id {id}")
                
                # Enregistre le client distant
                self.users[self.__nbr_id] = client
                
                # Démarre le loop script de reception
                self.__recv(id)
                
            except TimeoutError:
                pass
            
            except Exception as e:
                raise e
    

class SockerClient:
    """Version client de Socker"""
    
    def __init__(self, hostname, port):
        self.client = socket(AF_INET, SOCK_STREAM)
        self.client.connect((hostname, port))
        self.proto_host = AsymetricRSAPublicKey(self.client.recv(5210))
        self.rsa_key = AsymetricRSA()
        self.client.send(self.rsa_key.get_public_key().export_key())
    
    def send(self, message):
        msg = self.proto_host.encode(message)
        self.client.send(msg)
        
    def recv(self, size = 5120):
        msg_enc = self.client.recv(size)
        msg_clair = self.rsa_key.decode(msg_enc)
        return msg_clair

    def recvfile(self, size = 5120, step=2048):
        json_recv = self.recv(5120).decode()
        data_file = json.loads(json_recv)
        size = int(data_file["size"])
        filename = data_file["name"]
        key = self.recv(size)
        fernet = Fernet(key)
        
        with open(filename, "wb") as file:
            for i in range(size//step+1):
                fd = self.client.recv(2808)
                file.write(fernet.decrypt(fd))
                if len(fd.decode()) < 2808:
                    break


class SimpleSockerClient(SockerClient):
    
    def __init__(self, hostname, port):
        """Version sans chiffrements de SockerClient"""
        self.client = socket(AF_INET, SOCK_STREAM)
        self.client.connect((hostname, port))
    
    def recv(self, size = 5120):
        return self.client.recv(size)
    
    def recvfile(self, size = 5120, step=5120):
        json_recv = self.recv(5120).decode()
        data_file = json.loads(json_recv)
        size = int(data_file["size"])
        filename = data_file["name"]
        
        with open(filename, "wb") as file:
            while path.getsize(filename) < size:
                fd = self.client.recv(5120)
                file.write(fd)
        
        
if __name__ == "__main__":
    try:
        if param[1] == "host":
            host = Socker(verbose=True)
            sleep(5)
            host.send("Hello Client, I'm Host".encode(), 1)
            
        elif param[1] == 'client':
            client = SockerClient("localhost", 5051)
            sleep(1)
            client.send("Hello Host, I'm client")
            print(client.recv())
            
    except IndexError:
        print("Host or Client ?")