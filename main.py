from socker import Socker, SimpleSocker, SockerClient, SimpleSockerClient
from sys import argv as param


def main(host, port):
    client = Socker(host, port)


if __name__ == "__main__":
    if len(param) >= 1:
        if param[1] == 'host':
            verbose = False
            
            if '-v' in param or '--verbose' in param:
                verbose=True
            if '--no-rsa' in param:
	            host = SimpleSocker(verbose=verbose)
            else:
                host = Socker(verbose=verbose)
        
        elif param[1] == 'client':
            if "--no-rsa" in param:
                try:
                    SimpleSockerClient(
                        param[2], #hostname
                        param[3]  #port
                    )
                    
                except IndexError:
                    print("Usage: main.py client HOST PORT <option>")
            else:
                try:
                    main(
                        param[2], #hostname
                        param[3]  #port
                    )
                    
                except IndexError:
                    print("Usage: main.py client HOST PORT <option>")
                
                
    else:
        print('')
        print('Usage: main.py [host/client]')
